import React, { useState } from 'react';
import Weather from './Weather'
import axios from 'axios'

function App() {
  const [temp, setTemp] = useState()

  const getTemp = async () => {
    const newTemp = await axios.get('https://www.voacap.com/geo/weather.html?city=Vaasa')
    setTemp(newTemp.data[0].temperature)
  }

  setInterval(getTemp, 1500)

  return (
    <div className="App">
      <Weather temp={temp}/>
    </div>
  );
}

export default App;
